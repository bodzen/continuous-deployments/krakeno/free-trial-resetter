CREATE USER resetter_user WITH encrypted password 'USER PASSWORD';

/* Grant privileges for existing tables */
GRANT SELECT, INSERT, UPDATE, DELETE
ON ALL TABLES IN SCHEMA public
TO resetter_user;

/* Create related role with limited rights */
CREATE ROLE resetter_role WITH LOGIN;
GRANT resetter_role to zeus;
GRANT resetter_role to resetter_user;

/* Grant privileges for all future table create */
ALTER DEFAULT PRIVILEGES
	  FOR ROLE resetter_role
	  IN SCHEMA public
	  GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO resetter_user;

/* Grant privileges for all sequences */
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO resetter_user;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO resetter_role;
